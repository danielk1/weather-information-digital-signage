<?php
//include 'krumo/class.krumo.php';
include 'getWeather.php';
$forecast = getWeather();
$bodyClasses = 'current';
if (isset($forecast['alerts'])) {
	$alertDisplay = array();
	$newAlert = array();
	$alerts = $forecast['alerts'];
	foreach($alerts as $i => $alert){
		if (is_array($alert)){
			if (strpos($alert['title'], 'Lancaster')) {
				//This alert applies to Lancaster
				$bodyClasses .= ' alerts';
				$alertDisplay[] = $alert;
			} else {
				//This alert may not apply to Lancaster. Don't show it.
				unset($alerts[$i]);
			}
			
		} else {
			if (strpos($alert->title, 'Lancaster')) {
				//This alert applies to Lancaster
				$bodyClasses .= ' alerts';
				foreach($alert as $alertElement) {
					$newAlert[] = $alertElement;
				}
				$alertDisplay[] = $newAlert;
			} else {
				//This alert may not apply to Lancaster. Don't show it.
				unset($alerts->$i);
			}
		}
		
	}
}
?>

<html>
	<head>
		<title>Current Weather</title>
		<link rel="stylesheet" href="../css/current.css">
		<meta name="viewport" content="width=device-width">
		
	</head>
	
	<body class="<?php print $bodyClasses; ?>">
<!--		<p style="position: absolute">last updated: <?php print $forecast['lastUpdatedDisplay']; ?> <br>next update due: <?php print $forecast['nextUpdate']; ?><br>time to next update: <?php print $forecast['timeToUpdate']; ?> minutes</p>-->
		<div id="cloudBg">
			<h1>Currently</h1>

			<div id="temp"><?php print round($forecast['temperature']); ?><span>&deg;</span></div>
			<!--<div id="summary" class="<?php print $forecast['icon']; ?>"><div id="icon"></div><?php print mb_convert_encoding($forecast['summary'],'HTML-ENTITIES','utf-8') ; ?></div>-->
			<div id="summary" class="<?php print $forecast['icon']; ?>"><div id="icon"></div><h4 class="dailyForecastHeading">Today's Forecast</h4><?php print mb_convert_encoding($forecast['forecastToday'],'HTML-ENTITIES','utf-8') ; ?></div>
            <div id="statusBlock">
				<div id="feelsLike">Feels Like <?php print round($forecast['feelsLike']); ?>&deg;</div>
				<!--<div id="chanceOfPrecip">Chance of precipitation: <?php print round($forecast['precipitation']*100); ?>%</div>-->
              <ul id="otherDetails">
                <li>Wind: <?php print round($forecast['windSpeed']); ?>MPH</li>
                <li>&#124;</li>
                <li>Humidity: <?php print round($forecast['humidity']*100); ?>%</li>
              </ul>
			</div>
		</div>
	
	<?php if ($alerts): ?>
		<div id="marquee2" class="marqueeContainer" onmouseover="zxcMarquee.scroll('marquee2',0);" onmouseout="zxcMarquee.scroll('marquee2',-1);" >
			<div style="position: absolute; width: 4000px;">
				<p style="margin-top: 0"> <strong><?php print $alertDisplay[0]['title']; ?>:</strong> <?php print $alertDisplay[0]['description']; ?></p>
			</div>
		</div>
		<script src="marquee.js"></script>
	<?php endif; ?>
	
	<div id="attribution"><p>Powered by Forecast</p></div>
	
	
		
	</body>

</html>