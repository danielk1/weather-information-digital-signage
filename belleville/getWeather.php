<?php
require_once 'forecast-php-master/src/Forecast/Forecast.php';
use Forecast\Forecast;
function getWeather() {
	$forecast = '';
	$lastUpdated = strtotime(file_get_contents('updated.txt'));
	$refreshTime = $lastUpdated +(60*5);
	$currentTime = time();
	$timeDiff = $currentTime - $refreshTime;

	$lastUpdatedDisplay = date('D, M d, Y h:ia', $lastUpdated);
	$nextUpdate = date('D, M d, Y h:ia', $refreshTime);
	$timeToUpdate = round(($timeDiff * (-1) / 60), 2);

	//Check if the store is open. If the current time is between opening and closing times, set $open to true
	$open = false;
	$opening = strtotime('6:30am', time());
	$closing = strtotime('8:30pm', time());
	if ($currentTime > $opening && $currentTime < $closing) {
		$open = true;
	}

	if ($timeDiff > 0 && $open == true) {
		$forecastObject = new Forecast('570a055848c2879aeea2aa96cbee5e55');
		//Get the current forecast for a given latitude and longitude
		//fc is Forecast Data
		$fd = $forecastObject->get('40.616266','-77.699901');

		$currently = $fd->currently;
		$daily = $fd->daily->data;
		$dailyForecast = array();
		foreach ($daily as $key => $days) {
			if ($key > 0 && $key < 6) {
				$workingForecast = array (
					'when' => date('l', $days->time),
					'precipitation' => $days->precipProbability,
					'tempMin' => $days->temperatureMin,
					'tempMax' => $days->temperatureMax,
					'icon' => $days->icon,
					'summary' => $days->summary
				);
				$dailyForecast[$key] = $workingForecast;
			}
		}	
		$forecast = array(
			'summary' => $currently->summary,
			'icon' => $currently->icon,
			'temperature' => $currently->temperature,
			'feelsLike' => $currently->apparentTemperature,
			'precipitation' => $currently->precipProbability,
			'windSpeed' => $currently->windSpeed,
			'humidity' => $currently->humidity,
			'forecastToday' => $fd->daily->data[0]->summary,
			'daily' => $dailyForecast
		);
		if (isset($fd->alerts)) {
			//$alertObject = file_get_contents('alerts.txt');
			//parse_str($alertObject, $alerts);
			$alerts = array();
			foreach ($fd->alerts as $alertItem){
				if(strpos($alertItem->title, 'Lancaster')){
					$array = get_object_vars($alertItem);
					$alerts[] = $array;
					//$alertsString = http_build_query($alerts); 
					//file_put_contents('alerts.txt', $alertsString);
				}
			}
			$forecast['alerts'] = $alerts;
		}
		$forecastString = http_build_query($forecast);  
		file_put_contents('forecast.txt', $forecastString);
		$lastUpdated = date(DATE_RSS, time());
		file_put_contents('updated.txt', $lastUpdated);
	} else {
		$forecastObject = file_get_contents('forecast.txt');
		parse_str($forecastObject, $forecast);
	}
	$forecast['lastUpdatedDisplay'] = $lastUpdatedDisplay;
	$forecast['nextUpdate'] = $nextUpdate;
	$forecast['timeToUpdate'] = $timeToUpdate;
	return $forecast;
}
?>