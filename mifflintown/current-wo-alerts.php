<?php
include 'krumo/class.krumo.php';
include 'getWeather.php';
$forecast = getWeather();
$bodyClasses = 'current';
krumo($forecast);
if ($forecast['alerts']) {
	$alertDisplay = array();
	$newAlert = array();
	$alerts = $forecast['alerts'];
	foreach($alerts as $i => $alert){
		if (is_array($alert)){
			if (strpos($alert['title'], 'Lancaster')) {
				//This alert applies to Lancaster
				$bodyClasses .= ' alerts';
				$alertDisplay[] = $alert;
			} else {
				//This alert may not apply to Lancaster. Don't show it.
				unset($alerts[$i]);
			}
			
		} else {
			if (strpos($alert->title, 'Lancaster')) {
				//This alert applies to Lancaster
				$bodyClasses .= ' alerts';
				foreach($alert as $j => $alertElement) {
					$newAlert[$j] = $alertElement;
				}
				$alertDisplay[] = $newAlert;
			} else {
				//This alert may not apply to Lancaster. Don't show it.
				unset($alerts->$i);
			}
		}
		
	}
}

require_once 'forecast-php-master/src/Forecast/Forecast.php';
use Forecast\Forecast;
$forecastObject = new Forecast('570a055848c2879aeea2aa96cbee5e55');
//Get the current forecast for a given latitude and longitude
//fc is Forecast Data
$fd = $forecastObject->get('40.2056239','-76.2212943');
krumo($fd);
foreach ($fd->alerts as $alertItem){
	$alerts = array();
	if(strpos($alertItem->title, 'Lancaster')){
		$array = get_object_vars($alertItem);
		$alerts[] = $array;
	}
}
krumo($alerts);
//krumo(utf8_encode($fd->daily->data[2]->summary));
//utf8_decode
$forecast_decoded = array(
		'summary' => utf8_decode($currently->summary),
		'icon' => utf8_decode($currently->icon),
		'temperature' => utf8_decode($currently->temperature),
		'feelsLike' => utf8_decode($currently->apparentTemperature),
		'precipitation' => utf8_decode($currently->precipProbability),
		'windSpeed' => utf8_decode($currently->windSpeed),
		'humidity' => utf8_decode($currently->humidity),
		'forecastToday' => utf8_decode($fd->daily->data[0]->summary),
		'daily' => $dailyForecast
	);
krumo($forecast_decoded);
?>

<html>
	<head>
		<title>Current Weather</title>
		<link rel="stylesheet" href="/css/current.css">
		<meta name="viewport" content="width=device-width">
		
	</head>
	
	<body class="current">
<!--		<p style="position: absolute">last updated: <?php print $forecast['lastUpdatedDisplay']; ?> <br>next update due: <?php print $forecast['nextUpdate']; ?><br>time to next update: <?php print $forecast['timeToUpdate']; ?> minutes</p>-->
		<div id="cloudBg">
			<h1>Currently</h1>

			<div id="temp"><?php print round($forecast['temperature']); ?><span>&deg;</span></div>
			<div id="summary" class="<?php print $forecast['icon']; ?>"><div id="icon"></div><?php print utf8_decode($forecast['forecastToday']); ?></div>
			<div id="statusBlock">
				<div id="feelsLike">Feels Like <?php print round($forecast['feelsLike']); ?>&deg;</div>
				<div id="chanceOfPrecip">Chance of precipitation: <?php print round($forecast['precipitation']*100); ?>%</div>
				<div id="wind">Wind: <?php print round($forecast['windSpeed']); ?>MPH</div>
				<div id="humidity">Humidity: <?php print round($forecast['humidity']*100); ?>%</div>
			</div>
		</div>
		
		<div id="attribution"><p>Powered by Forecast</p></div>
		
	</body>


</html>