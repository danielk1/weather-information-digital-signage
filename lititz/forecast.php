<?php
include 'getWeather.php';
$forecast = getWeather();
?>

<html>
	<head>
		<title>5 Day Forecast</title>
		<link rel="stylesheet" href="../css/forecast.css">
		<meta name="viewport" content="width=device-width">
	</head>
	
	<body class="forecast">
		<div id="blocksBg">
			<?php foreach($forecast['daily'] as $key => $day): ?>
			<div class="day" id="<?php print 'day'.$key ?>">
				<h2><?php print $day['when']; ?></h2>
				<div class="summary <?php print $day['icon']; ?>"><div id="icon"></div><?php print mb_convert_encoding($day['summary'],'HTML-ENTITIES','utf-8'); ?></div>
				<div class="high">High: <?php print round($day['tempMax']); ?>&deg;</div>
			<div class="low">Low: <?php print round($day['tempMin']); ?>&deg;</div>
			<div class="chanceOfPrecip">Precip: <?php print round($day['precipitation']*100); ?>%</div>
			</div>
			<?php endforeach;?>

		</div>
		
		<div id="attribution"><p>Powered by Forecast</p></div>
		
	</body>


</html>